import React, { useState } from 'react';
import {
    FaTh,
    FaBars,
    FaUserAlt,
    FaShoppingBag
}from "react-icons/fa";
import { NavLink } from 'react-router-dom';


const Sidebar = ({children}) => {
    const[isOpen ,setIsOpen] = useState(false);
    const toggle = () => setIsOpen (!isOpen);
    const menuItem=[
        {
            path:"/",
            name:"Dashboard",
            icon:<FaTh/>
        },
        {
          path:"/product",
          name:"Product",
          icon:<FaShoppingBag/>
        },
        {
            path:"/supplier",
            name:"Supplier",
            icon:<FaUserAlt/>
        }
    ]
    return (
        <div className="flex">
           <div style={{width: isOpen ? "300px" : "50px"}} className=" w-screen h-screen bg-grey text-black transition-all border-r border-gray-200">
               <div className="flex  items-center py-5 px-4">
                   <h1 style={{display: isOpen ? "block" : "none"}} className="text-3xl">SIGUDANG</h1>
                   <div style={{marginLeft: isOpen ? "50px" : "0px"}} className="flex text-2xl ml-17.5">
                       <FaBars onClick={toggle}/>
                   </div>
               </div>
               {
                   menuItem.map((item, index)=>(
                       <NavLink to={item.path} key={index} className="flex text-black py-2.5 px-4 gap-4 transition-all hover:bg-sky-500 hover:text-black hover:transition-all active:bg-sky-500 active:text-black">
                           <div className="text-xl">{item.icon}</div>
                           <div style={{display: isOpen ? "block" : "none"}} className="text-xl">{item.name}</div>
                       </NavLink>
                   ))
               }
           </div>
           <main className='w-screen bg-grey'>{children}</main>
        </div>
    );
};

export default Sidebar;