import { Link } from "react-router-dom";

const isProduct = (product) => {
    return product.map((data, i) => {
        return (
            <tr key={i}>
                <th>{data.id}</th>
                <td>{data.name}</td>
                <td>{data.description}</td>
                <td>{data.qty}</td>
                <td>{data.price}</td>
                <td>
                    <div className="badge badge-outline ">
                        <Link as='button'>
                            Edit
                        </Link>
                    </div>
                    <div className="badge badge-outline">
                        <Link  method='get' data={{ id: product.id }} as='button'>
                            Delete
                        </Link>
                    </div>
                </td>
            </tr>
        )
    })
}

const noProduct = () => {
    return (
        <div>Tidak ada product</div>
    )
}

const ProductList = ({ product }) => {
    return !product ? noProduct() : isProduct(product)
}
export default ProductList