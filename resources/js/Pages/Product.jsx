import ProductList from '@/Components/ProductList';
import React, { useState } from 'react';
import { AiOutlinePlus } from "react-icons/ai";
import { Inertia } from '@inertiajs/inertia';

const Product = (props) => {
    const [toggle, setToggle] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [qty, setQty] = useState('');
    const [price, setPrice] = useState('');

    const handleSubmit = () => {
        const data = {
            name, description, qty, price
        }
        Inertia.post('/addProduct', data)
        setName('');
        setDescription('');
        setQty('');
        setPrice('');
        isTogle()
    }

    const isTogle = () => {
        setToggle(!toggle)
    }
    return (
        <div className='m-8'>

            <div className='flex mb-5'>
                <div className='mb-5'>
                    <button className="btn btn-outline" onClick={isTogle}> <AiOutlinePlus /> Tambah Produk</button>
                </div>
                {toggle &&
                    <>
                        <div className='ml-10 w-6/12 mb-5'>
                            <div className=' flex items-center justify-between'>
                                <div className='mr-20'>
                                    <p>Nama Produk :</p>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Nama Produk"
                                    className="input input-bordered w-full max-w-xs"
                                    onChange={(name) => setName(name.target.value)}
                                    value={name} 
                                    required/>
                            </div>
                            <div className='flex items-center justify-between'>
                                <div className='mr-20'>
                                    <p>Deskripsi Produk :</p>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Deskripsi Produk"
                                    className="input input-bordered w-full max-w-xs"
                                    onChange={(description) => setDescription(description.target.value)}
                                    value={description} 
                                    required/>
                            </div>
                            <div className=' flex items-center justify-between'>
                                <div className='mr-20'>
                                    <p>Kuantias Produk :</p>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Kuantitas Produk"
                                    className="input input-bordered w-full max-w-xs"
                                    onChange={(qty) => setQty(qty.target.value)}
                                    value={qty} 
                                    required/>
                            </div>
                            <div className=' flex items-center justify-between'>
                                <div className='mr-20'>
                                    <p>Harga Produk :</p>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Harga Produk"
                                    className="input input-bordered w-full max-w-xs"
                                    onChange={(price) => setPrice(price.target.value)}
                                    value={price} 
                                    required/>
                            </div>
                        </div>
                        <div className='relative ml-10 mb-5'>
                            <button
                                className=" absolute bottom-0 btn btn-inline bg-black text-white"
                                onClick={() => handleSubmit()}
                            >
                                Simpan
                            </button>
                        </div>
                    </>
                }
            </div>
            <div className="overflow-auto block h-96 border b-black">
                <table className="table table-zebra table-stripped">
                    <thead className='bg-black text-white'>
                        <tr className=''>
                            <th>ID</th>
                            <th>Nama Barang</th>
                            <th>Deskripsi</th>
                            <th>Kuantitas</th>
                            <th>Harga</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <ProductList product={props.product.data} />
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default Product;