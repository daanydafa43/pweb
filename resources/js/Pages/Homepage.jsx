import Sidebar from "@/Components/Sidebar";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from "./Dashboard";
import NavBar from "@/Components/NavBar";
import Supplier from "./Supplier";
import Product from "./Product";

export default function Homepage(props) {
    console.log(props)
    return (
        <BrowserRouter>
            <Sidebar>
                <NavBar user={props.auth.user} />
                <Routes>
                    <Route path="/" element={<Dashboard />} />
                    <Route path="/dashboard" element={<Dashboard />} />
                    <Route path="/product" element={<Product product={props.product}/>} />
                    <Route path="/supplier" element={<Supplier />} />
                </Routes>
            </Sidebar>
        </BrowserRouter>
    )
}